var domain = 'https://player.me';
var pageSize = 50;
var currentPage = 1;
var currentUser = null;
var currentSource = "";
var currentDirection = "";
var activities = [];

function setStatusMessage(message){
	$("#statusText").html(message);
}

function addActivity(activity)
{
	activities.push(activity);
	
	var item;
	var post = "";
	var activityType = activity.type.toLowerCase();
	
	switch(activityType){
		case 'post':
			post = activity.data.post;
			if (activity.data.metas && activity.data.metas.length)
			{
				post +=	'<ul class="metas">';
				for (var mi=0; mi < activity.data.metas.length; mi++)
				{
					var meta = activity.data.metas[mi];
					post +=	'<div class="meta"><a href="'+meta.url+'">';
					if (meta.thumbnail) post +=	'<img class="thumbnail" src="https:'+meta.thumbnail+'"><br />';
					if (meta.title) post += meta.title;
					post += '</a>';
					if (meta.description) post += '<br />'+meta.description;
					post +=	'</div>';
				}
				post +=	'</ul>';
			}
			break;
		case 'video':
			post = '';
			if (activity.data.thumbnail) post+= '<img class="thumbnail" src="https:'+activity.data.thumbnail+'">';
			post += '<a href="'+activity.data.url+'">'+activity.data.title+'</a>';
			if (activity.data.description) post += '<br />'+activity.data.description;
			break;
		default:
			item = '<p>Error: Unhandled type "'+activity.type+'".</p>';
	}
	
	if (!item)
	{
		var parsedPublishTime = Date.parse(activity.published_at);
		var publishedTime = new Date(parsedPublishTime);

		var source 		= activity.source;
		var userUrl 	= domain + currentUser.slug;
		var avatarSrc 	= 'https:' + currentUser.avatar.cached;
		var username 	= currentUser.username;
		var sourceUrl 	= activity.full_url;
		var timeUrl 	= activity.full_url;
		var time 		= publishedTime.toLocaleString();	//TODO: Process timestamp?
		var numLikes 	= activity.likesCount;
		var numComments	= "???";					//TODO: Do we want this?

		item =	'<div class="'+activityType+' activity card source-'+source+'">';
		
		item +=		'<div class="info">';
		item +=			'<a href="'+userUrl+'"><img class="avatar" src="'+avatarSrc+'"><span class="username">'+username+'</span></a>';
		item +=			'<div class="source"><a href="'+sourceUrl+'"><span class="source-label"> #'+activities.length+' posted from </span><span class="source-icon picon"></span></a></div>';
		item +=		'</div>';
		
		item +=		'<div class="content"><div class="post">'+post+'</div></div>';
		
		item +=		'<div class="footer">';
		item +=			'<div class="time"><a href="'+timeUrl+'"><span class="picon icon-clock"></span><span>'+time+'</span></a></div>';
		item +=			'<div class="container"><span class="like"><i class="picon"></i>'+numLikes+'</span></div>';
	//	item +=			'<div class="container"><span class="comment"><i class="picon"></i>'+numComments+'</span></div>';
		item +=		'</div>';
		
		item +=	'</div>';
	}
	
	$("<li>"+item+"</li>").appendTo("#list");
}

function clear()
{
	currentPage = 1;
	currentUser = null;
	activities = [];
	$("#list").empty();
	
	$("#moreBtn").prop('disabled', true);
	
	setStatusMessage("Please select a user and click 'Go!'");
}

function getUserActivities(user, page, sourceType, direction)
{
	if (!user) return setStatusMessage("No user selected");
	if (typeof type == "undefined") type = "";
	if (typeof direction == "undefined") direction = "";
	
	currentUser = user;
	currentPage = page;
	currentSource = sourceType;
	currentDirection = direction;

	setURLHash({
		'username': user.slug,
		'order': direction
	});
	
	var url = domain;
	url += '/api/v1/users/'+user.id+'/activities?_limit='+pageSize+'&_page='+page+'&_dir='+direction;
//	if (sourceType) url += '&type='+sourceType; //Not working?
	url += '&callback=?';

	$("#moreBtn").prop('disabled', true);
	setStatusMessage("Getting activity for "+user.username+"...");
	
	$.getJSON(url).done(function(data)
	{
		if (!data || !data.success || !data.results) return setStatusMessage("Failed to get user activities");
		onUserActivities(data.results, data.pager);
	})
	.fail(function(error){
		setStatusMessage("Failed to get user activities");
	});
}

function onUserActivities(results, pager)
{
	var totalActivities = pager.total;
	var numActivitiesBefore = activities.length;

	var msPerDay = 24*60*60*1000;
	var msUserCreatedAt = Date.parse(currentUser.created_at);
	var daysPassed = Math.round((Date.now() - msUserCreatedAt) / msPerDay);
	var activitiesPerDay = Math.round(totalActivities/daysPassed*10000)/10000;
	var averageMessage = currentUser.username+" averages "+activitiesPerDay+" activities per day";

	for (var i=0; i < results.length; i++) addActivity(results[i]);

	if (activities.length >= numActivitiesBefore + pageSize){
		$("#moreBtn").prop('disabled', false);
		setStatusMessage(averageMessage+". "+activities.length+"/"+totalActivities+" loaded.");
	}else if(activities.length){
		setStatusMessage(averageMessage+". All "+activities.length+" loaded!");
	}else{
		setStatusMessage("No activity found.");
	}
}

function onUser(data)
{
	if (!data || !data.success || !data.results) return setStatusMessage("Failed to get user");
	if (data.results.length==0) return setStatusMessage("The user was not found.");

	var inputSource = "";
	var inputDirection = $("#sortDirection").val();
	
	return getUserActivities(data.results, 1, inputSource, inputDirection);
}

function fillSelf()
{
	if ($("#inputField").val()) return;

	clear();
	setStatusMessage("Searching for your user");
	var url = domain+'/api/v1/users/default?callback=?';
	
	$.getJSON(url).done(function(data){
		var inputField = $("#inputField");
		if (inputField.val()){
			//Something's in the input field
		}else if (data && data.success && data.results ){
			inputField.val(data.results.username);
			setStatusMessage("Hello, "+data.results.username);
		}else{
			setStatusMessage("Looks like you're not logged in. Please enter a username.");
		}
	});
}

function getUser(inputString)
{
	clear();
	var queryString = encodeURIComponent(inputString);
	setStatusMessage("Searching for "+inputString);
	
	var url = domain+'/api/v1/users/'+queryString+'?username=true&callback=?';
	
	$.getJSON(url).done(function(data){
		if (!data || !data.success){
			if(typeof data.results=="string") return setStatusMessage("Error: "+data.results);
			return setStatusMessage("Error: Failed to get user");
		}
		onUser(data);
	})
	.fail(function(error){
		setStatusMessage("Error: Failed to get "+inputString);
	});
}

function hashToObject(){
	var result = {};
	var str = window.location.hash;

	if (str.length >= 4) str = str.substring(1);
	else return null;
	
	var hashArr = str.split("&");
	for (var i = 0; i < hashArr.length; i++)
	{
		var pair = hashArr[i].split("=");
		if (pair.length<2) continue;

		var key = pair[0];
		var value = pair[1];

		for (var k=2; k < pair.length; k++) value += "="+pair[k];
		result[key] = value;
	}
	return result;
}
function setURLHash(object){
	var hash = "";
	for (var key in object)
	{
		var value = object[key];
		if (hash) hash += "&";
		hash += key+"="+value;
	}
	window.location.hash = hash;
}
function updateURLHash(property, value){
	var obj = hashToObject();
	if (!obj) obj = {};

	obj[property] = value;
	setURLHash(obj);
}

$(function()
{
	var $inputField = $("#inputField");

	$("#goBtn").click(function()
	{
		var inputName = $("#inputField").val();
		
		if (!inputName) setStatusMessage("Invalid name");
		else getUser(inputName);
	});
	
	$("#moreBtn").click(function(){
		getUserActivities(currentUser, currentPage+1, currentSource, currentDirection);
	});

	$inputField.keypress(function(e) {
		if (e.which == 13) {
			$("#goBtn").click();
			e.preventDefault();
		}
	});
	
	clear();
	var hashObj = hashToObject();

	if (hashObj && hashObj.hasOwnProperty('username')){

		$inputField.val(hashObj.username);

		if (hashObj.hasOwnProperty('order')){
			$("#sortDirection").val(hashObj.order);
		}

		getUser(hashObj.username);
	}else{
		fillSelf();
	}
});
